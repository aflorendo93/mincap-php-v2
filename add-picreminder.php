<?php

require "template/template.php";
function getTitle(){
    echo "[wkly] | Add [pkly]";
}
function getContent(){
require "controllers/connection.php"


?>

<div class="d-flex justify-content-center align-items-center flex-column">
    <div class="row container-fluid">
        <div class="col-lg-6 offset-lg-1">
            <img class="img-fluid mt-3" src="assets/images/add-img.png" alt="">
        </div>
        <div class="col-lg-3">
            <h1 class="pt-5 pb-1"><img class="img-fluid mb-3" src="assets/images/addpicreminder.png" alt="Add PicReminder"></h1>
            <form action="controllers/process_picreminder.php" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="lead" for="taskDate">Date :</label>
                    <input class="form-control" type="search" id="datepicker" name="taskDate" autocomplete="off" placeholder="Click to show Calendar">
                    <input type="hidden" id="day" name="taskDay">
                </div>
                <div class="form-group">
                    <label class="lead" for="description">Image Description:</label>
                    <textarea name="description" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label class="lead" for="imgPath"> Image:</label>
                    <input type="file" name="imgPath" class="form-control">
                </div>
                <button class="btn btn-primary btn-block my-5" type="submit">Add List</button>
            </form>
        </div>
    </div>
</div>   





<?php

};

?>
<?php
require "template/template.php";
function getTitle(){
    echo "[wkly] | Pick A Date";
}
function getContent(){
?>

    <div class="d-flex justify-content-center align-items-center flex-column">
        <div class="row container-fluid">
            <div class="col-lg-6 offset-lg-1">
                <img class="img-fluid mt-3" src="assets/images/add-img.png" alt="">
            </div>
            <div class="col-lg-3">
                <h1 class="pt-5 pb-1"><img class="img-fluid" src="assets/images/pick-a-date.png" alt="Pick A Date"></h1>

                <form action="controllers/process_add_date.php" method="post">
                    <div class="form-group mt-5">
                        <label class="lead" for="taskDate">Task Date :</label>
                        <input class="form-control" type="search" id="datepicker" name="taskDate" autocomplete="off" placeholder="Click to show Calendar">
                        <input type="hidden" id="day" name="taskDay">
                        <button class="btn btn-primary btn-block my-5" type="submit">Next: Add ToDos</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php
};

?>

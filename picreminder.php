<?php
require "template/template.php";
function getTitle(){
    echo "[wkly] | My PicReminders";
}
function getContent(){
require "controllers/connection.php"

?>
<!-- <div class="container"> -->
        <div class="col-lg-12">
            <h1 class="text-center display-3 mt-5"><img class="img-fluid" src="assets/images/mypicreminder.png" alt=""></h1>
            <div class="text-center display-4"id="date"></div>
            
            <div class="row">

                <?php

                    $userId = $_SESSION['user']['id'];

                    $picreminders_query = "SELECT * FROM picreminders WHERE user_id = $userId ORDER BY taskDate ASC";

                    $picreminders = mysqli_query($conn, $picreminders_query);
                    foreach($picreminders as $indivPicrem){
                ?>

                <div class="col-lg-2 ">
                    <div class="card text-center px-3 pt-3 pb-2">
                        <h3 class=<?php echo $indivPicrem['status'] === '1'? " text-danger": ""?>><?php echo $indivPicrem['taskDay']?></h3>
                        <h4 class=<?php echo $indivPicrem['status'] === '1'? " text-danger": ""?>><?php echo date("F j, Y", strtotime($indivPicrem['taskDate']))?></h4>
                        <img class="card-img-top border border-primary" height="200px" src="<?php echo $indivPicrem["imgPath"]?>"><br>
                        <span>Note:</span>
                        <span><?php echo $indivPicrem["description"]?></span>
                    </div>
                    <div class="card-footer text-center">
                        <?php
                            if($indivPicrem['status'] === '0'){
                        ?>
                        <a class="mr-5 btn btn-block btn-primary mb-1" href="controllers/process_mark.php?picId=<?php echo $indivPicrem['id']?>&status=<?php echo $indivPicrem['status']?>">Remind Me</a>
                        <?php
                            }else {
                        ?>
                        <a class="mr-5 btn btn-warning
                        " href="controllers/process_mark.php?picId=<?php echo $indivPicrem['id']?>&status=<?php echo $indivPicrem['status']?>">Forget Me</a>
                        <?php
                            };
                        ?>
                        <a class="" href="edit-picreminder.php?picId=<?php echo $indivPicrem['id']?>"><i class="fa fa-pencil-square-o btn-outline-warning" aria-hidden="true"></i></a>
                        <a class="mt-5" href="controllers/process_delete_picreminder.php?picId=<?php echo $indivPicrem['id']?>"><i class="fa fa-minus-circle btn-outline-danger" aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php
                    };

                ?>
        </div>
<!-- </div> -->

<?php
};

?>

<?php 

session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootswatch -->
    <link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.css">
    
    <!-- styling -->
    <link rel="stylesheet" href="../assets/styles/style.css">

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>

    <!--  toastr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" defer></script>

    <!--  Registration Validation Script-->
    <script src="../assets/scripts/register.js" defer></script>

    <!-- Datepicker Jquery -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js" defer></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" defer></script>
    <script src="../assets/scripts/datePicker.js" defer></script>

    
    <!-- Strikethrough -->
    <link rel="stylesheet" href="../assets/styles/strikethrough.css">

    <!--  toastr css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- fontawesome -->
    <script src="https://kit.fontawesome.com/805cad612c.js" crossorigin="anonymous"></script>
  

    <title><?php getTitle(); ?></title>
</head>
<body>

<?php

    require "navbar.php";

?>

<?php
    getContent();
?>



</body>
</html>
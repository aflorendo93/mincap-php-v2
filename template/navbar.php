
<nav class="navbar navbar-expand-sm navar-inner pb-3">
  <a class="navbar-brand" href="../index.php"><img src="../assets/images/logo-sm.png" alt=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
        <?php

        if(!isset($_SESSION['user'])){

        ?>
        <li class="nav-item">
          <a class="nav-link" href="../login.php">Login</a>
        </li>      
        <li class="nav-item">
          <a class="nav-link" href="../register.php">Register</a>
        </li>
        <?php
        } else{
        ?>
        <li class="nav-item active">
          <a class="nav-link" href="../todolist.php">My ToDos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../add-date.php">Add New ToDos</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="../picreminder.php">My [pkly]</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../add-picreminder.php">Add New [pkly]</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
        </li>
        <?php
        };
        ?>
      </ul>
  </div>
</nav>
<hr class="nav-hr m-0">
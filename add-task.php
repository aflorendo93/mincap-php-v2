<?php

require "template/template.php";
function getTitle(){
    echo "[wkly] | Add ToDos";
}

function getContent (){

?>

<div class="d-flex justify-content-center align-items-center flex-column">
    <div class="row container-fluid">
        <div class="col-lg-6 offset-lg-1">
            <img class="img-fluid mt-3" src="assets/images/add-img.png" alt="">
        </div>
        <div class="col-lg-3">
            <h1 class="pt-5 pb-1"><img class="img-fluid" src="assets/images/what-to-do.png" alt="Pick A Date"></h1>
            <form action="controllers/process_add_task.php" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="lead" for="firstTask">Task 1:</label>
                    <input type="text" name="firstTask" class="form-control">
                </div>
                <div class="form-group">
                    <label class="lead" for="secondTask">Task 2:</label>
                    <input type="text" name="secondTask" class="form-control">
                </div>
                <div class="form-group">
                    <label class="lead" for="thirdTask">Task 3:</label>
                    <input type="text" name="thirdTask" class="form-control">
                </div>
                <button class="btn btn-primary btn-block my-5" type="submit">Let's Do This</button>
            </form>
        </div>   
    </div>
</div>  





<?php

};

?>
<?php
    require "template/template.php";
    function getTitle(){
        echo "[wkly] | Register";
    }
    function getContent() {
?>
    <div class="d-flex justify-content-center align-items-center flex-column">
        <div class="row container-fluid">
            <div class="col-lg-6 offset-lg-1">
                <img class="img-fluid mt-3" src="assets/images/register-img-q.png" alt="">
            </div>
        <div class="col-lg-3 offset-lg-1 col-md-6 offset-md-1">
            <h1 class="pt-5 pb-1"><img src="assets/images/register.png" alt="Register"></h1>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="lead" for="firstName"><span class='text-danger'>*</span> First Name:</label>
                    <input type="text" name="firstName" class="form-control" id="firstName">
                    <span class="text-danger"></span>
                </div>
                <div class="form-group">
                    <label class="lead" for="lastName"><span class='text-danger'>*</span> Last Name:</label>
                    <input type="text" name="lastName" class="form-control" id="lastName">
                    <span class="text-danger"></span>
                </div>
                <div class="form-group">
                    <label class="lead" for="email"><span class='text-danger'>*</span> E-mail:</label>
                    <input type="email" name="email" class="form-control" id="email">
                    <span class="text-danger"></span>
                </div>
                <div class="form-group">
                    <label class="lead" for="password"><span class='text-danger'>*</span> Password:</label>
                    <input type="password" name="password" class="form-control" id="password">
                    <span class="text-danger"></span>
                </div>
                <div class="form-group">
                    <label class="lead" for="confirm_password"><span class='text-danger'>*</span>Confirm Password:</label>
                    <input type="password" name="confirm_password" class="form-control" id="confirmPassword">
                    <span class="text-danger"></span>
                </div>
                <button type="button" class="btn btn-primary btn-block mt-5" id="registerBtn">Register</button>
            </form>
            <p class="py-2">Already registered?<a href="login.php" class="text-danger"> Login here.</a></p>
        </div>
        </div>
    </div>


<?php
};

?>

<footer>
    <div class="footer-copyright text-center py-3 bg-dark text-light fixed-bottom">© 2020 Copyright: zuittB67 [Argie, Julie, Kaycee]</div>
</footer>
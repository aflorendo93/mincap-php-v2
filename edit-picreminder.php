<?php

require "template/template.php";
function getTitle(){
    echo "[wkly] | Edit [pkly]";
}

function getContent() {
require "controllers/connection.php";


?>
    <div class="d-flex justify-content-center align-items-center flex-column">
        <div class="row container-fluid">
            <div class="col-lg-6 offset-lg-1">
                <img class="img-fluid mt-3" src="assets/images/edit-img.png" alt="">
            </div>
            <div class="col-lg-3">
                <h1 class="pt-5 pb-1"><img class="img-fluid" src="assets/images/editpicreminder.png" alt="Edit PicReminder"></h1>
        <form action="controllers/process_edit_picreminder.php" method="POST" enctype="multipart/form-data">
        <?php
            $picId = $_GET['picId'];
            $userId = $_SESSION['user']['id'];

            $get_picreminder_query = "SELECT * FROM picreminders WHERE id = $picId AND user_id = $userId";
            $get_picreminder = mysqli_fetch_assoc(mysqli_query($conn, $get_picreminder_query));
        ?>
            <div class="form-group">
                <label class="lead" for="taskDate">Date :</label>
                <input class="form-control" type="search" id="datepicker" name="taskDate" autocomplete="off" value="<?php echo $get_picreminder['taskDate'] ?>">
                <input type="hidden" id="day" name="taskDay" value="<?php echo $get_picreminder['taskDay'] ?>">
            </div>
            <div class="form-group">
                <label class="lead" for="description">Image Description:</label>
                <textarea name="description" class="form-control"><?php echo $get_picreminder['description'] ?></textarea>
            </div>
            <div class="form-group">
                <label class="lead" for="imgPath"> Image:</label>
                <img src="<?= $get_picreminder['imgPath']?>" height="50px" width="50px" alt="">
                <input type="file" name="imgPath" class="form-control">
            </div>
            <input type="hidden" name="picId" value="<?php echo $picId ?>">
            <button class="btn btn-primary btn-block my-5" type="submit">Save Changes</button>
        </form>
    </div>   
    </div>
</div> 


<?php
};

?>
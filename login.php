<?php
require "template/template.php";
function getTitle(){
    echo "[wkly] | Login";
}
function getContent(){

?>

    <div class="d-flex justify-content-center align-items-center flex-column">
        <div class="row container-fluid">
            <div class="col-lg-6 offset-lg-1">
                <img class="img-fluid mt-3" src="assets/images/login-img-q.png" alt="">
            </div>
            <div class="col-lg-3 offset-lg-1">
                <h1 class="pt-5 pb-1"><img src="assets/images/login.png" alt="Login"></h1>
                <form action="controllers/process_login.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="lead" for="email">Email:</label>
                        <input type="email" name="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label class="lead" for="password">Password:</label>
                        <input type="password" name="password" class="form-control" id="password">
                        <p><a href="#" class="text-muted">Forgot your Password?</a></p>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block mt-5" id="loginBtn">Login</button>
                </form>
                <p class="py-3">New User? <a href="register.php" class="text-danger">Register here.</a></p>
            </div>
        </div>
    </div>


<?php
};

?>

<footer>
    <div class="footer-copyright text-center py-3 bg-dark text-light fixed-bottom">© 2020 Copyright: zuittB67 [Argie, Julie, Kaycee]</div>
</footer>
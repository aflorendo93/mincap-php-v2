<?php
require "template/template.php";
function getTitle(){
    echo "[wkly] | My ToDos";
}
function getContent(){
require "controllers/connection.php"
?>

<div class="col-lg-12">
<h1 class="text-center display-3 mt-5 mb-2"><img class="img-fluid" src="assets/images/mytodos.png" alt="My ToDos"></h1>  
<div class="text-center display-4"id="date"></div>
<div class="row">

    <?php

        $userId = $_SESSION['user']['id'];

        $dates_query = "SELECT DISTINCT dates.id, dates.taskDate, dates.taskDay FROM dates JOIN tasks ON (dates.id = tasks.date_id) WHERE user_id = $userId ORDER BY taskDate ASC";

        $dates = mysqli_query($conn, $dates_query);

        foreach($dates as $indivDate){
    ?>

    <div class="col-lg-2 ">
        <div class="card text-center pt-4 pb-4 mt-5 px-2">
            <?php $dateNow = date("Y-m-d") ?>
            <h3 class=<?php echo $indivDate['taskDate'] === $dateNow? " text-danger": "" ?>><?php echo $indivDate['taskDay']?></h3>
            <h4 class=<?php echo $indivDate['taskDate'] === $dateNow? " text-danger": "" ?>><?php echo date("F j, Y", strtotime($indivDate['taskDate']))?></h4>
            <table>
                    <thead class="pb-5">
                        <th class="m-0 pb-2">Tasks:</th>
                        <hr class="m-0 p-0">
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                <?php
                    $tasks_query = "SELECT tasks.id, dates.taskDate, dates.taskDay, tasks.task, tasks.status FROM tasks JOIN dates ON (dates.id = tasks.date_id) WHERE tasks.user_id = $userId";

                    $tasks = mysqli_query($conn, $tasks_query);

                    foreach($tasks as $indivTask){
                    if($indivDate['taskDate'] === $indivTask['taskDate']){
                ?>

                        <tr>
                            <td width=45%><span class=<?php echo $indivTask['status'] === '0'? "" : " mark"?>><?php echo $indivTask['task']?></td>
                        <?php
                            if($indivTask['status'] === '0'){
                            ?>
                            <td width=10%><a href="controllers/process_change_status.php?status=<?php echo $indivTask['status']?>&id=<?php echo $indivTask['id']?>&user_id=<?php echo $userId ?>"><i class="fa fa-check btn-outline-success" aria-hidden="true"></i></a></td>
                            <?php
                            }else{
                            ?>
                            <td width=10%><a href="controllers/process_change_status.php?status=<?php echo $indivTask['status']?>&id=<?php echo $indivTask['id']?>&user_id=<?php echo $userId ?>"><i class="fa fa-refresh btn-outline-info" aria-hidden="true"></i></a></td>
                            <?php
                            };
                            ?>
                            <td width=10%><a href="edit-task.php?taskId=<?php echo $indivTask['id']?>&taskInfo=<?php echo $indivTask['task'] ?>&taskDate=<?php echo $indivTask['taskDate']?>&taskDay=<?php echo $indivTask['taskDay']?>"><i class="far fa-edit btn-outline-warning"></i></a></td>
                            <td width=10%><a href="controllers/process_delete_task.php?id=<?php echo $indivTask['id'] ?>"><i class="far fa-trash-alt btn-outline-danger"></i></a></td>
                        <tr>

                    <?php
                        };
                    };
                                                        
                    ?>
                </tbody>
            </table>
        </div>

        <div class="card-footer text-center">
            <a class="mr-5" href="add-more-task.php?datesId=<?php echo $indivDate['id']?>&userId=<?php echo $userId ?>"><i class="fa fa-plus btn-outline-info" aria-hidden="true"><i class="fa fa-plus btn-outline-info" aria-hidden="true"></i></i></a>

            <a class="ml-5" href="controllers/process_delete_list.php?taskDate=<?php echo $indivDate['taskDate'] ?>"><i class="fa fa-minus-circle btn-outline-danger" aria-hidden="true"></i></a>

        </div>
    </div>
    <?php
        };

    ?>

    </div>
    <div class="d-flex align-items-center justify-content-center">
        <a href="add-date.php"><button type="submit" class="btn btn-primary mt-5" id="loginBtn">Add To do List</button></a>
    </div>
</div>

<?php
};

?>
</a>
<?php

require "template/template.php";
function getTitle(){
    echo "[wkly] | Do More";
}
function getContent (){

?>

<div class="d-flex justify-content-center align-items-center flex-column">
        <div class="row container-fluid">
            <div class="col-lg-6 offset-lg-1">
                <img class="img-fluid mt-3" src="assets/images/edit-img.png" alt="">
            </div>
            <div class="col-lg-3">
                <h1 class="pt-5 pb-1"><img class="img-fluid" src="assets/images/do-more.png" alt="Login"></h1>


                <form action="controllers/process_add_more_task.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="lead" for="firstTask">Add On Task 1:</label>
                        <input type="text" name="firstTask" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="lead" for="secondTask">Add On Task 2:</label>
                        <input type="text" name="secondTask" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="lead" for="thirdTask">Add On Task 3:</label>
                        <input type="text" name="thirdTask" class="form-control">
                    </div>
                    <?php
                        $userId = $_GET['userId'];
                        $dateId = $_GET['datesId'];
                    ?>
                    <input type="hidden" name="userId" value="<?php echo $userId ?>">
                    <input type="hidden" name="dateId" value="<?php echo $dateId ?>">
                    <button class="btn btn-primary btn-block my-5" type="submit">Let's Do This</button>
                </form>
                
            </div>        
        </div>
</div>    





<?php

};

?>
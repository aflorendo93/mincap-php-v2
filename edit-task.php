<?php

require "template/template.php";
function getTitle(){
    echo "[wkly] | Edit ToDos";
}

function getContent() {


?>
    <div class="d-flex justify-content-center align-items-center flex-column">
        <div class="row container-fluid">
            <div class="col-lg-6 offset-lg-1">
                <img class="img-fluid mt-3" src="assets/images/edit-img.png" alt="">
            </div>
            <div class="col-lg-3">
                <h1 class="pt-5 pb-1"><img class="img-fluid" src="assets/images/edit.png" alt="Pick A Date"></h1>
        <form action="controllers/process_edit_task.php" method="POST" enctype="multipart/form-data">
        <?php
            $taskId = $_GET['taskId'];
            $taskInfo = $_GET['taskInfo'];
            $taskDate = $_GET['taskDate'];
            $taskDay = $_GET['taskDay'];
        ?>
            <div class="form-group">
                <label class="lead" for="taskDate">Date:</label>
                <input type="text" name="taskDate" class="form-control" value ="<?php echo $taskDate ?>" disabled>
            </div>
            <div class="form-group">
                <label class="lead" for="taskDay">Day:</label>
                <input type="text" name="taskDay" class="form-control" value ="<?php echo $taskDay ?>" disabled>
            </div>
            <div class="form-group">
                <label class="lead" for="taskEdit">Task to be edited:</label>
                <input type="text" name="taskEdit" class="form-control" value ="<?php echo $taskInfo?>" disabled>
            </div>
            <div class="form-group">
                <label class="lead" for="newTask">Change it to:</label>
                <input type="text" name="newTask" class="form-control">
            </div>
            <input type="hidden" name="taskId" value="<?php echo $taskId ?>">
            <button class="btn btn-primary btn-block my-5" type="submit">Save Changes</button>
        </form>
    </div>   
    </div>
</div> 


<?php
};

?>
<?php
require "template/landing.php";
function getTitle(){
    echo "Welcome to [wkly]";
}
    function getLanding(){

?>
<nav class="navbar navbar-expand-sm navbar-inner navrtl">
        <div class="collapse navbar-collapse" id="navbarColor03">
            <div class="navrtl">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link btn btn-danger px-3" href="register.php">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#how-it-works">How it works</a>
                </li>
                </ul>
            </div>
    </div>
</nav>


<!-- <div class="container"> -->

    <div class="my-2 mx-0 pb-5">
            <h3 class="ml-2">todo list without complexity</h3>
            <div class="">
                <img class="img-fluid" src="assets/images/logo-bg.png" alt="[wkly]">
            </div>
            
                <?php
                    if(!isset($_SESSION['user'])){
                ?>
            <div class="ml-5 mt-3">
                <a href="register.php"><button class="btn btn-danger pt-4"><h3 class="text-light">Sign Up for Free</h3></button></a>
                <blockquote class="blockquote mt-3">Already have an account? <u class="text-danger"><a href="login.php" class="text-danger">Login here.</a></u></blockquote>
            </div>
                
                <?php
                    }else {
                ?>

            <div class="offset-1 mt-3">
                <h1>WELCOME<?php echo " ". $_SESSION['user']['firstName']. "!" ?></h1>
                <a href="todolist.php"><button class="btn btn-danger pt-4"><h3 class="text-light">Go! do your lists</h3></button></a>
            </div>     

            <?php
            };
            ?>
           
    
            <div class="jumbotron p-5 m-5">
            <h1 class="display-1 p-5 m-5"> </h1>
            </div>
            <div class="jumbotron p-5 m-5">
            <h1 class="display-1 p-5 m-5"> </h1>
            </div>
            <div class="jumbotron p-5 m-5">
            <h1 class="display-1 p-5 m-5"> </h1>
            </div>
            <div class="jumbotron p-5 m-5">
            <h1 class="display-1 p-5 m-5"> </h1>
            </div>

            <!-- <div>
                <img class="float-right img-fluid" src="assets/images/quote-gray.png" alt="">
            </div> -->
            
            
            <div class="jumbotron p-5 m-5">
            <h1 class="display-1 p-5 m-5"> </h1>
            </div>
            <div class="jumbotron p-0 m-0">
            </div>

            <!-- <div class="container-fluid p-0 m-0">
                <div class="row">
                    <div class="col-lg-12 text-center">
                    <img src="assets/images/features.png" alt="placeholder 960" class="img-fluid" />
                    </div>
                </div>
            </div> -->


            <div class="d-flex justify-content-center align-items-center flex-column">
                <div class="row container-fluid">
                    <div class="col-lg-12 text-center">
                    <img class="float-right img-fluid" src="assets/images/quote-gray.png" alt="">
                            <img class="img-fluid mt-3" src="assets/images/features2.png" alt="">
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center mt-5" id="how-it-works">
                <button class="btn btn-danger p-4">
                    Discover how [wkly] works
                </button>
            </div>
            


            <div class="jumbotron p-5 m-5">
            <h1 class="display-1 p-5 m-5"> </h1>
            </div>


            <div class="text-center">
                <img class="img-fluid" src="assets/images/twitter/testimonials.png" alt="testimonials">
            </div>

            <div class="d-flex justify-content-center align-items-center flex-column">
                <div class="row container-fluid">
                    
                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid mt-3" src="assets/images/twitter/sirred.png" alt="">
                    </div>
                
                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid mt-3" src="assets/images/twitter/dom.png" alt="">
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid mt-3" src="assets/images/twitter/pia.png" alt="">
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid mt-3" src="assets/images/twitter/jet.png" alt="">
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid mt-3" src="assets/images/twitter/mark.png" alt="">
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <img class="img-fluid mt-3" src="assets/images/twitter/arvin.png" alt="">
                    </div>

                </div>
            </div>
    </div>  <!-- my-2 mx-0 -->


<!-- </div>  container  -->


<?php

};
?>

<footer>
    <div class="footer-copyright text-center py-3 bg-dark text-light">© 2020 Copyright: zuittB67 [Argie, Julie, Kaycee]</div>
</footer>
$(function() {
    $("#datepicker").datepicker({
    dateFormat: "yy-mm-dd",
    onSelect: function(dateText){
      var seldate = $(this).datepicker('getDate');
      seldate = seldate.toDateString();
      seldate = seldate.split(' ');
      var weekday=new Array();
          weekday['Mon']="Monday";
          weekday['Tue']="Tuesday";
          weekday['Wed']="Wednesday";
          weekday['Thu']="Thursday";
          weekday['Fri']="Friday";
          weekday['Sat']="Saturday";
          weekday['Sun']="Sunday";
      var dayOfWeek = weekday[seldate[0]];
      $('#day').val(dayOfWeek);
    }
    });
  } );

  
  // SHOW TODAY'S DATE

  const options = {weekday: 'long', month: 'long', day: 'numeric'};
  const today = new Date();
  
  const dateElement = document.getElementById("date");
  
  // let options = {weekday: 'long', monthly: 'short', day: 'numeric'};
  
  // let today = new Date();
  
  dateElement.innerHTML = "TODAY: " + today.toLocaleDateString("en-US", options);
  
  
  
  

